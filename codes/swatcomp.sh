ifort -c hru_module.f90 -traceback -O3 -parallel
ifort -c time_module.f90 -traceback -O3 -parallel
ifort -c constituent_mass_module.f90 -traceback -O3 -parallel
ifort -c *_module.f90 -traceback -O3 -parallel
ifort -c allocate_parms.f90 -traceback -O3 -parallel
ifort -c *.f90 -traceback -O3 -parallel
ifort -o swatplus_exe_file_name *.o -static -traceback -O3 -parallel