      subroutine res_hydro (jres, id, ihyd, pvol_m3, evol_m3)

      use reservoir_data_module
      use reservoir_module
      use conditional_module
      use climate_module
      use time_module
      use hydrograph_module
      use water_body_module
      
      implicit none
      
      real,  intent (in) :: pvol_m3
      real,  intent (in) :: evol_m3
      integer,  intent (in) :: jres             !none      |hru number
      integer :: nstep            !none      |counter
      integer :: tstep            !none      |hru number
      integer :: iac              !none      |counter 
      integer :: ic              !none      |counter
      integer,  intent (in) :: id               !none      |hru number
      integer :: ial              !none      |counter
      integer :: irel             !          |
      integer :: iob              !none      |hru or wro number
      integer,  intent (in) :: ihyd             !          |
      real :: vol                 !          |
      real :: b_lo                !          |
      character(len=1) :: action  !          |
      real :: res_h               !          |
      real :: demand              !m3        |irrigation demand by hru or wro
      real :: temp_flo_out
      real :: dvol_coef           !none      |coef for drawdown volume definition
      real :: b_day            !none      |Dynamic day, how many days left till target date
      
      !! store initial values
      vol = wbody%flo
      nstep = 1

      do tstep = 1, nstep
          
        !calc release from decision table
        do iac = 1, dtbl_res(id)%acts
          action = "n"
          if (dtbl_res(id)%alts == 0) then
            action = "y"
          else
            do ial = 1, dtbl_res(id)%alts
              if (dtbl_res(id)%act_hit(ial) == "y" .and. dtbl_res(id)%act_outcomes(iac,ial) == "y") then
                action = "y"
                exit
              end if
            end do
          end if
          
          !condition is met - set the release rate
          if (action == "y") then
            select case (dtbl_res(id)%act(iac)%option)
            case ("rate")
              ht2%flo = dtbl_res(id)%act(iac)%const * 86400.
              
            case ("inflo_rate")
              ht2%flo = amax1 (ht1%flo, dtbl_res(id)%act(iac)%const * 86400.)
              
            case ("days")
              select case (dtbl_res(id)%act(iac)%file_pointer)
                case ("null")
                  b_lo = 0.
                case ("pvol")
                  b_lo = pvol_m3
                case ("evol")
                  b_lo = evol_m3
              end select
              ht2%flo = (wbody%flo - b_lo) / dtbl_res(id)%act(iac)%const
            
            case ("keep") ! Keep volume at certain value: pvol, evol, dvol. Qout should be in [MinQ, MaxQ] which are [Const2, Const]
              dvol_coef = dtbl_res(id)%cond(3)%lim_const
              select case (dtbl_res(id)%act(iac)%file_pointer)
                case ("null")
                  b_lo = 0.
                case ("pvol")
                  b_lo = pvol_m3
                case ("evol")
                  b_lo = evol_m3
                case ("dvol")
                  b_lo = pvol_m3 * dvol_coef
                end select
              
              ! To set a outflow limitation
              temp_flo_out = wbody%flo - b_lo
              if (temp_flo_out < dtbl_res(id)%act(iac)%const2 * 86400.) then
                ht2%flo = dtbl_res(id)%act(iac)%const2 * 86400.
              else if (temp_flo_out > dtbl_res(id)%act(iac)%const * 86400.) then
                ht2%flo = dtbl_res(id)%act(iac)%const * 86400.
              else
                ht2%flo = temp_flo_out
              end if
              
            case ("dday") ! Designed for winter drawdown. Qout = (vol - b_lo)/(target_day - day_now). Qout should be in [MinQ, MaxQ] which are [Const2, Const]
              dvol_coef = dtbl_res(id)%cond(3)%lim_const
                select case (dtbl_res(id)%act(iac)%file_pointer)
                case ("null")
                  b_lo = 0.
                case ("pvol")
                  b_lo = pvol_m3
                case ("evol")
                  b_lo = evol_m3
                case ("dvol")
                  b_lo = pvol_m3 * dvol_coef
                end select
             ! Calculate how many day left; Target day is the 7th condition
              if (dtbl_res(id)%cond(7)%lim_const - Real(time%day) < 0.) then ! when the target day is in next year
                  b_day = 366.0 - Real(time%day) + dtbl_res(id)%cond(7)%lim_const
              else if (dtbl_res(id)%cond(7)%lim_const - Real(time%day) == 0.) then
                  b_day = 0.2
              else
                  b_day = dtbl_res(id)%cond(7)%lim_const - Real(time%day)
              end if
             ! Calculate release rate
             temp_flo_out = (wbody%flo - b_lo) / b_day + ht1%flo
              if (temp_flo_out < dtbl_res(id)%act(iac)%const2 * 86400.) then ! must release higher than the min outflow and inflow
                 ht2%flo = dtbl_res(id)%act(iac)%const2 * 86400.
              else if (temp_flo_out > dtbl_res(id)%act(iac)%const * 86400.) then
                 ht2%flo = dtbl_res(id)%act(iac)%const * 86400.
              else
                 ht2%flo = temp_flo_out
              end if
              
            case ("rday") ! Designed for refill. Qout = (vol - b_lo)/(target_day - day_now). Qout should be in [MinQ, MaxQ] which are [Const2, Const]
              dvol_coef = dtbl_res(id)%cond(3)%lim_const
                select case (dtbl_res(id)%act(iac)%file_pointer)
                case ("null")
                  b_lo = 0.
                case ("pvol")
                  b_lo = pvol_m3
                case ("evol")
                  b_lo = evol_m3
                case ("dvol")
                  b_lo = pvol_m3 * dvol_coef
                end select
             ! Calculate how many day left; Target day is the 5th condition
              if (dtbl_res(id)%cond(5)%lim_const - Real(time%day) < 0.) then ! when the target day is in next year
                  b_day = 366.0 - Real(time%day) + dtbl_res(id)%cond(5)%lim_const
              else if (dtbl_res(id)%cond(5)%lim_const - Real(time%day) == 0.) then
                  b_day = 0.2
              else
                  b_day = dtbl_res(id)%cond(5)%lim_const - Real(time%day)
              end if
             ! Calculate release rate
             temp_flo_out = ht1%flo + (wbody%flo - b_lo) / b_day
              if (temp_flo_out < dtbl_res(id)%act(iac)%const2 * 86400.) then ! must release higher than the min outflow
                 ht2%flo = dtbl_res(id)%act(iac)%const2 * 86400.
              else if (temp_flo_out > dtbl_res(id)%act(iac)%const * 86400.) then
                 ht2%flo = dtbl_res(id)%act(iac)%const * 86400.
              else
                 ht2%flo = temp_flo_out
              end if
              
              
            case ("dyrt")
              !for base volume for drawdown days, use condition associated with action
              select case (dtbl_res(id)%act(iac)%file_pointer)
                case ("con1")
                  ic = 1
                case ("con2")
                  ic = 2
                case ("con3")
                  ic = 3
                case ("con4")
                  ic = 4
              end select
              !perform operation on target variable to get target
              select case ((d_tbl%cond(ic)%lim_op))
              case ('=') 
                b_lo = pvol_m3 + (evol_m3 - pvol_m3) * d_tbl%cond(ic)%lim_const
              case ("*")
                b_lo = (evol_m3 - pvol_m3) * d_tbl%cond(ic)%lim_const
              case ("+")
                b_lo = (evol_m3 - pvol_m3) + d_tbl%cond(ic)%lim_const
              case ("-")
                b_lo = (evol_m3 - pvol_m3) - d_tbl%cond(ic)%lim_const
              case ("/")
                b_lo = (evol_m3 - pvol_m3) / d_tbl%cond(ic)%lim_const
              end select
              ht2%flo = (wbody%flo - b_lo) / dtbl_res(id)%act(iac)%const +          &
                                      dtbl_res(id)%act(iac)%const2 * pvol_m3 / 100.
              
            case ("irrig_dmd")
              iob = Int(dtbl_res(id)%act(iac)%const2)
              select case (dtbl_res(id)%act(iac)%file_pointer)
              case ("wro")    !demand from water resource object
                demand = wro(iob)%demand
              case ("hru")    !demand from single hru
                demand = irrig(iob)%demand
              end select
              !! const allows a fraction (usually > 1.0) of the demand (m3) released
              ht2%flo = demand * dtbl_res(id)%act(iac)%const
                 
            case ("weir")
              ht2%flo = res_weir(ihyd)%c * res_weir(ihyd)%k * res_weir(ihyd)%w * (res_h ** 1.5)
              
            case ("meas")
              irel = int(dtbl_res(id)%act_typ(iac))
              select case (recall(irel)%typ)
              case (1)    !daily
                ht2%flo = recall(irel)%hd(time%day,time%yrs)%flo
              case (2)    !monthly
                ht2%flo = recall(irel)%hd(time%mo,time%yrs)%flo
              case (3)    !annual
                ht2%flo = recall(irel)%hd(1,time%yrs)%flo
              end select
            end select
            
            !! write outflow details to reservoir release file
            ! res/hru number, action name, action option, const1, const2, outflow(m3), storage(m3), principle stor(m3), emergency stor(m3)
            ! jres, dtbl_res(id)%act(iac)%name, dtbl_res(id)%act(iac)%option, dtbl_res(id)%act(iac)%const,  &
            !  dtbl_res(id)%act(iac)%const2, ht2%flo, wbody%flo, pvol_m3, evol_m3
            
          end if
        end do    ! iac

      end do    !tstep loop

      return
      end subroutine res_hydro